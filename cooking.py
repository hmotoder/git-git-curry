import json
import os
from functools import reduce


CURRY_MENU = os.environ.get('CURRY_MENU', 0)
CURRY_LIMIT = os.environ.get('CURRY_LIMIT', 0)
NONE_FOOD_PARAMETER = { 'l':0, 't':0, 'k':0 }


def get_foods():
	with open('foods/foods.json', 'r') as f:
		return json.load(f)['foods']


def get_food_parameter(food):
	# t: Taste, b: Balance, k: Kind(R=Rice, M=Meat, V=Vegetable, S=Seafood, I=Ingredient)
	mapping_table = {
		'rice': 	 { 't': 0, 'b': 0, 'k': 'R' },
		'beef': 	 { 't': 3, 'b': 2, 'k': 'M' },
		'chicken':	 { 't': 2, 'b': 2, 'k': 'M' },
		'pork':		 { 't': 1, 'b': 2, 'k': 'M' },
		'shrimp':	 { 't': 3, 'b': 2, 'k': 'S' },
		'scallops':	 { 't': 2, 'b': 2, 'k': 'S' },
		'squid': 	 { 't': 1, 'b': 2, 'k': 'S' },
		'onion':	 { 't': 3, 'b': 2, 'k': 'V' },
		'carrot':	 { 't': 2, 'b': 2, 'k': 'V' },
		'potato': 	 { 't': 1, 'b': 2, 'k': 'V' },
		'milk':		 { 't': 5, 'b': 3, 'k': 'I' },
		'apple':     { 't': 2, 'b': 3, 'k': 'I' },
		'chocolate': { 't': 5, 'b': 3, 'k': 'I' },
		'beer':      { 't': 5, 'b': 3, 'k': 'I' },
		'yogurt':    { 't': 5, 'b': 2, 'k': 'I' },
		'honey':     { 't': 2, 'b': 2, 'k': 'I' },
		'love':		 { 't': 6, 'b': 5, 'k': 'I' },
	}
	parameter = mapping_table.get(food, NONE_FOOD_PARAMETER)
	return parameter


def curry_filter(curry, food_parameter):
	ret = str(curry) + food_parameter['k']
	if curry is 0:
		return NONE_FOOD_PARAMETER
	elif ret in ['1M', '2S', '3V']:
		food_parameter['b'] = 0
	return food_parameter


def total_food_parameter(sum, element):
	filtered = curry_filter(CURRY_MENU, element)
	sum['t'] += filtered['t']
	sum['b'] += filtered['b']
	sum['k'].append(filtered['k'])
	return sum


def eat_curry(curry):
	print("----------------------------------------------")
	if curry['t'] == 0:
		print("何も具材がない！😡")
	elif curry['b'] > int(CURRY_LIMIT) or 'R' not in curry['k']:
		print("これはカレーじゃない何かだ🤢")
	else:
		print("美味しい🍛 （", curry['t'], 'pt', "）")

if CURRY_MENU is 0 and CURRY_LIMIT is 0:
	print("お客さんこないなあ")
else:
	foods = get_foods()
	food_params = list(map(get_food_parameter, foods))
	curry = reduce(total_food_parameter, food_params, {'t':0, 'b':0, 'k': []})
	eat_curry(curry)
